import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class InputTest {

    @Test
    void inputtitles() {
        Input input = new Input();
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        System.setIn(strIn);
        input.inputfile();

        //assertEquals(input.getIgnores(),2);
        String inputStr = "Descent of Man" + "\n"+
                "The Ascent of Man"+ "\n"+
                "The Old Man and The Sea"+ "\n"+
                "A Portrait of The Artist As a Young Man"+ "\n"+
                "A Man is a Man but Bubblesort IS A DOG";

        assertEquals(input.returnLine(),inputStr);

    }
    @Test
    void inputIgnores(){
        Input input = new Input();
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" ;
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        System.setIn(strIn);
        input.inputfile();

        ArrayList<String> actualIgnore = new ArrayList<String>();
        actualIgnore.add("is");
        actualIgnore.add("the");
        actualIgnore.add("of");
        actualIgnore.add("and");
        actualIgnore.add("as");
        actualIgnore.add("a");
        actualIgnore.add("but");

        assertEquals(actualIgnore,input.getIgnores());
    }
}