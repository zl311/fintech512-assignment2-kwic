import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class LinesWithKeword {

    private ArrayList<Line> linesKey;

    public LinesWithKeword(Input input) {
        ArrayList<String> Ignores = input.getIgnores();
        ArrayList<String> lines = input.getLines();
        ArrayList<ArrayList<String>> words = input.getWords();

        this.linesKey = new ArrayList<Line>();
        for (int i = 0; i < words.size(); i++) {
            for(int j=0; j < words.get(i).size();j++){
                if(!Ignores.contains(words.get(i).get(j).toLowerCase())){
                    String key = words.get(i).get(j).toUpperCase();
                    linesKey.add(new Line(mergeWords(words.get(i),j),key));

                }
            }
        }

    }

    public String mergeWords(ArrayList<String> Words, int n) {
        String line = "";
        for (int i=0;i< Words.size();i++) {
            if(i==n){
                line+=Words.get(i).toUpperCase();
                line+=" ";
            }
            else{
                line+=Words.get(i).toLowerCase();
                line+=" ";
            }
        }
        line = line.substring(0,line.length() - 1);
        return line;

    }
    public String returnLine(){
        String line = "";
        for(int i=0;i<this.linesKey.size();i++){
            //System.out.println(linesKey.get(i).line);
            line+=linesKey.get(i).line;
            line+="\n";
        }
        line = line.substring(0,line.length() - 1);
        return line;
    }
    public void printLine(){
        for(int i=0;i<this.linesKey.size();i++){
            System.out.println(linesKey.get(i).line);
        }
    }

    public void sortLine(){
        linesKey.sort(new Comparator<Line>() {
            @Override
            public int compare(Line o1, Line o2) {
                return o1.keyword.compareTo(o2.keyword);

            }
        });

    }


}
